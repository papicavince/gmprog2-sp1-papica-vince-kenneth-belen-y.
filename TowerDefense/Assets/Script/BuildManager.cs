﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildManager : MonoBehaviour 
{
	public static BuildManager instance;
	private TurretBlueprint turretToBuild;
	public GameObject arrowTurretPrefab;
	public GameObject cannonTurretPrefab;
    public GameObject fireTurretPrefab;
    public GameObject iceTurretPrefab;
    private Node selectedNode;
    public NodeUI nodeUI;

	void Awake ()
	{
		if (instance != null)
		{
			return;
		}
		instance = this;
	}

    public void SelectNode (Node node)
    {
        if (selectedNode == node)
        {
            DeselectNode();
            return;
        }

        selectedNode = node;
        turretToBuild = null;

        nodeUI.SetTarget(node);
    }

    public void DeselectNode()
    {
        selectedNode = null;
        nodeUI.Hide();
    }

	public void SelectTurretToBuild (TurretBlueprint turret)
	{
		turretToBuild = turret;
        DeselectNode();
	}

    public TurretBlueprint GetTurretToBuild()
    {
        return turretToBuild;
    }

	public bool CanBuild { get { return turretToBuild != null; } }
    public bool HasMoney { get { return PlayerStats.Money >= turretToBuild.cost; } }
}
