﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public enum BulletType { Arrow, Cannon, Fire, Ice }

    private Transform target;

    public float speed = 70f;
    public int damage = 50;
    public int dpsDamage = 5;
    public float freezeAmount = 0.75f;
    public float radius = 0f;
    public float damageOverTime = 0.3f;
    public int ticks = 5;
    public GameObject impactEffect;

    public BulletType bulletType = BulletType.Arrow;

    public void Seek(Transform _target)
    {
        target = _target;
    }

    // Update is called once per frame
    void Update()
    {
        if (target == null)
        {
            Destroy(gameObject);
            return;
        }

        Vector3 dir = target.position - transform.position;
        float distanceThisFrame = speed * Time.deltaTime;

        if (dir.magnitude <= distanceThisFrame)
        {
            HitTarget();
            return;
        }

        transform.Translate(dir.normalized * distanceThisFrame, Space.World);
    }

    void HitTarget()
    {
        GameObject effect = (GameObject)Instantiate(impactEffect, transform.position, transform.rotation);
        Destroy(effect, 1f);

        if (bulletType == BulletType.Arrow) Damage(target);
        else if (bulletType == BulletType.Cannon)
        {
            Explode();
            //Damage(target);
        }
        else if (bulletType == BulletType.Fire)
        {
            //Debug.Log("BulletType.Fire");
            BurnAOE();
        }
        else if (bulletType == BulletType.Ice)
        {
            FreezeAOE();
        }

        Destroy(gameObject);
    }

    void Explode()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, radius);
        foreach (Collider collider in colliders)
        {
            if (collider.tag == "Enemy Fly" || collider.tag == "Enemy Ground")
            {
                Debug.Log("Explode");
                Damage(collider.transform);
            }
        }
    }

    void BurnAOE()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, radius);
        foreach (Collider collider in colliders)
        {
            if (collider.tag == "Enemy Fly" || collider.tag == "Enemy Ground")
            {
                Burn(collider.transform);
            }
        }
    }

    void Burn(Transform enemy)
    {
        EnemyStats e = enemy.GetComponent<EnemyStats>();

        if (e != null)
        {
            //Debug.Log("Burn");
            e.CallTakeDamageOverTime(dpsDamage, damageOverTime, ticks);
            e.TakeDamage(damage);
        }
    }
    void FreezeAOE()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, radius);
        foreach (Collider collider in colliders)
        {
            if (collider.tag == "Enemy Fly" || collider.tag == "Enemy Ground")
            {
                Freeze(collider.transform);
            }
        }
    }

    void Freeze(Transform enemy)
    {
        EnemyStats e = enemy.GetComponent<EnemyStats>();

        if (e != null)
        {
            e.CallFreezeOverTime(freezeAmount, damageOverTime, ticks);
            e.TakeDamage(damage);
        }
    }

    void Damage(Transform enemy)
    {
        EnemyStats e = enemy.GetComponent<EnemyStats>();

        if (e != null) e.TakeDamage(damage);
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, radius);
    }
}
