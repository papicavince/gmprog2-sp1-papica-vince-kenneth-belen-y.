﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyStats : MonoBehaviour
{
    public float speed = 5f;
    private float startSpeed;

    private Transform target;
    private int wavepointIndex = 0;

    public int maxHealth = 100;
    public float curHealth;
    public int value = 50;

    public Image healthBar;

    public int fireStack = 0;
    private bool onFire = false;

    // Use this for initialization
    void Start()
    {
        target = Waypoints.points[0];
        curHealth = maxHealth;
        startSpeed = speed;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 dir = target.position - transform.position;
        transform.Translate(dir.normalized * speed * Time.deltaTime, Space.World);

        if (Vector3.Distance(transform.position, target.transform.position) <= 0.4f) GetNextWaypoint();
    }

    public void TakeDamage(int amount)
    {
        //Debug.Log("Taken Damage");
        curHealth -= amount;

        healthBar.fillAmount = curHealth / maxHealth;

        if (curHealth <= 0) Die();
    }

    public void CallTakeDamageOverTime(int amount, float time, int maxTicks)
    {
        StartCoroutine(TakeDamageOverTime(amount, time, maxTicks));
    }

    public IEnumerator TakeDamageOverTime(int amount, float time, int maxTicks)
    {
        for (int i = 0; i < maxTicks; i++)
        {
            fireStack++;
            curHealth -= amount * fireStack;

            Debug.Log(amount * fireStack);
            healthBar.fillAmount = curHealth / maxHealth;

            yield return new WaitForSeconds(time);
        }
    }

    public void CallFreezeOverTime(float amount, float time, int maxTicks)
    {
        StartCoroutine(FreezeOverTime(amount, time, maxTicks));
    }


    public IEnumerator FreezeOverTime(float slowAmount, float time, int ticks)
    {
        for (int i = 0; i < ticks; i++)
        {
            speed *= slowAmount;

            yield return new WaitForSeconds(time);
            Debug.Log("StartSpeed");
            speed = startSpeed;
        }
    }

    public float NormalizedHealth()
    {
        return curHealth / maxHealth;
    }

    void GetNextWaypoint()
    {
        if (wavepointIndex >= Waypoints.points.Length - 1)
        {
            EndPath();
            return;
        }

        wavepointIndex++;
        target = Waypoints.points[wavepointIndex];
    }

    void EndPath()
    {
        PlayerStats.Lives--;
        WaveSpawner.EnemiesAlive--;
        Destroy(gameObject);
    }

    public void Die()
    {
        PlayerStats.Money += value;
        Destroy(gameObject);
        WaveSpawner.EnemiesAlive--;
    }
}
