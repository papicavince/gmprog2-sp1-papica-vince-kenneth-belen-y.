﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Node : MonoBehaviour 
{
	[Header("Render")]
	public Color hoverColor;
	public Color invalidColor;
    public Color notEnoughMoneyColor;
	public float transparency = 0.5f;
	private Color startColor;
	private Renderer rend;
	private bool turnOn;

	[HideInInspector]
	public GameObject turret;
	[HideInInspector]
	public TurretBlueprint turretBlueprint;
	[HideInInspector]
	public bool isUpgraded = false;

	[Header("Transform")]
	public Vector3 offset;

	BuildManager buildManager;
	private bool occupied;

	public int turretLevel = 1;
	
	void Start()
	{
		rend = GetComponent<Renderer>();
		startColor = rend.material.color;
		rend.material.color = hoverColor;

		buildManager = BuildManager.instance;

		turnOn = true;
		occupied = false;
		MeshRenderer(!turnOn);
	}

	public Vector3 GetBuildPosition ()
	{
		return transform.position + offset;
	}

	void OnMouseDown() 
	{
		if (EventSystem.current.IsPointerOverGameObject()) return;
		
        //if (!buildManager.HasMoney) rend.material.color = notEnoughMoneyColor;

        if (turret != null)
        {
            buildManager.SelectNode(this); 
            return;
        }
        if (!buildManager.CanBuild) return;
        BuildTurret(buildManager.GetTurretToBuild());

		occupied = true;
	}

    void BuildTurret (TurretBlueprint blueprint)
    {
        if (PlayerStats.Money < blueprint.cost)
        {
            Debug.Log("Not enough money to build");
            return;
        }
        PlayerStats.Money -= blueprint.cost;

		GameObject _turret = (GameObject)Instantiate (blueprint.prefab, GetBuildPosition(), Quaternion.identity);
		turret = _turret;

		turretBlueprint = blueprint;

        Debug.Log("Turret built");
    }

	public void UpgradeTurret()
	{
		//if (turretBlueprint.turretLevel >= 3) return;
		if (PlayerStats.Money < turretBlueprint.upgradeCost)
        {
            Debug.Log("Not enough money to upgrade");
            return;
        }
        PlayerStats.Money -= turretBlueprint.upgradeCost;

		Destroy(turret);

		GameObject _turret = (GameObject)Instantiate (turretBlueprint.upgradedPrefab, GetBuildPosition(), Quaternion.identity);
		turret = _turret;	

		isUpgraded = true;
		//turretBlueprint.turretLevel++;

        Debug.Log("Turret upgraded ");
	}

	void OnMouseEnter()
	{
		if (EventSystem.current.IsPointerOverGameObject()) return;
		if (!buildManager.CanBuild) return;
        if (!buildManager.HasMoney) rend.material.color = notEnoughMoneyColor;
        if (!occupied) rend.material.color = hoverColor;
        else if (occupied) rend.material.color = invalidColor;
		MeshRenderer(turnOn);
	}


	void OnMouseExit()
	{
		MeshRenderer(!turnOn);
	}

	void MeshRenderer (bool on)
	{
		MeshRenderer mr = gameObject.GetComponent<MeshRenderer>();

		if (on) mr.enabled = true;
		else if (!on)  mr.enabled = false;
	}
}
