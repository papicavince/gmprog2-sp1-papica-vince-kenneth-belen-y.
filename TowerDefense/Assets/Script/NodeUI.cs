﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class NodeUI : MonoBehaviour
{
    public GameObject UI;

    public Text upgradeCost;

    private Node target;

    public void SetTarget (Node _target)
    {
        target = _target;

        transform.position = target.GetBuildPosition();      

        upgradeCost.text = "₱" + target.turretBlueprint.upgradeCost;

        UI.SetActive(true);
    }

    public void Hide()
    {
        UI.SetActive(false);
    }

    public void Upgrade ()
    {
        if (target.turretLevel == 3) return;
        target.UpgradeTurret();
        target.turretLevel++;
        BuildManager.instance.DeselectNode();
    }
}
