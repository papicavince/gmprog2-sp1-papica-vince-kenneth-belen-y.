﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shop : MonoBehaviour
{
    public TurretBlueprint arrowTurretPrefab;
    public TurretBlueprint cannonTurretPrefab;
    public TurretBlueprint fireTurretPrefab;
    public TurretBlueprint iceTurretPrefab;

    BuildManager buildManager;

    void Start()
    {
        buildManager = BuildManager.instance;
    }

    public void SelectArrowTurret()
    {
        Debug.Log("Arrow Turret Selected");
        buildManager.SelectTurretToBuild(arrowTurretPrefab);
    }

    public void SelectCannonTurret()
    {
        Debug.Log("Cannon Turret Selected");
        buildManager.SelectTurretToBuild(cannonTurretPrefab);
    }

    public void SelectFireTurret()
    {
        Debug.Log("Fire Turret Selected");
        buildManager.SelectTurretToBuild(fireTurretPrefab);
    }

    public void SelectIceTurret()
    {
        Debug.Log("Ice Turret Selected");
        buildManager.SelectTurretToBuild(iceTurretPrefab);
    }
}
