﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : MonoBehaviour 
{
	public string towerType;
	private Transform target;

	[HideInInspector]
	public int level = 1;

	[Header("Attributes")]
	public float range = 15f;
	public float fireRate = 1f;
	private float fireCountdown = 0f;
	public string enemyTag = "Enemy";

	[Header("Unity Setup")]
	public Transform turretHead;
	public float turnSpeed = 10f;
	public GameObject bulletPrefab;
	public Transform firePoint;

	// Use this for initialization1
	void Start () 
	{
		InvokeRepeating("UpdateTarget", 0f, 0.5f);
	}
	
	public List<GameObject> FindTarget ()
	{
		List<GameObject> allEnemies = new List<GameObject>();
		List<GameObject> targets = new List<GameObject>();

		GameObject[] groundEnemies = GameObject.FindGameObjectsWithTag("Enemy Ground");
		GameObject[] flyEnemies = GameObject.FindGameObjectsWithTag("Enemy Fly");

		if (towerType == "Ground") 
		{
			foreach (GameObject enemy in groundEnemies) allEnemies.Add(enemy);
			targets = allEnemies;
		}
		else if (towerType == "Fly") 
		{
			foreach (GameObject enemy in groundEnemies) allEnemies.Add(enemy);
			foreach (GameObject enemy in flyEnemies) allEnemies.Add(enemy);
			targets = allEnemies;
		}

		return targets;
	}

	void UpdateTarget ()
	{
		// GameObject[] targets = GameObject.FindGameObjectsWithTag("Enemy Ground");

		List<GameObject> targets = new List<GameObject>();
		targets = FindTarget();

		float shortestDistance = Mathf.Infinity;
		GameObject nearestEnemy = null;
		foreach (GameObject enemy in targets)
		{
			float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);
			if (distanceToEnemy < shortestDistance)
			{
				shortestDistance = distanceToEnemy;
				nearestEnemy = enemy;
			}
		}

		if (nearestEnemy != null && shortestDistance <= range)
		{
			target = nearestEnemy.transform;
		} else
		{
			target = null;
		}
	}

	// Update is called once per frame
	void Update () 
	{
		if (target == null)
		{
			return;
		}

		LockOnTarget();

		if (fireCountdown <= 0f)
		{
			Shoot();
			fireCountdown = 1f / fireRate;
		}
		fireCountdown -= Time.deltaTime;
	}

	void Shoot ()
	{
		GameObject bulletGO = (GameObject)Instantiate (bulletPrefab, firePoint.position, firePoint.rotation);
		Bullet bullet = bulletGO.GetComponent<Bullet>();

		if (bullet != null) bullet.Seek(target);
	}

	void LockOnTarget ()
	{
		Vector3 dir = target.position - transform.position;
		Quaternion lookRotation = Quaternion.LookRotation(dir);
		Vector3 rotation = Quaternion.Lerp(turretHead.rotation, lookRotation, Time.deltaTime * turnSpeed).eulerAngles;
		turretHead.rotation = Quaternion.Euler(0f, rotation.y, 0f);
	}

	void OnDrawGizmosSelected ()
	{
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere(transform.position, range);
	}
}
