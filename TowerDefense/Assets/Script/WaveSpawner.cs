﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class WaveSpawner : MonoBehaviour
{
    public static int EnemiesAlive = 0;

    public GameObject BossGo;

    public Wave[] waves;

    public Transform spawnPoint;

    public float spawnWait = 5f;
    private float countdown = 2f;

    public Text waveCountdownText;

    private int waveIndex = 0;

    void Update()
    {
        if (EnemiesAlive > 0) return;

        if (countdown <= 0f)
        {
            StartCoroutine(SpawnWave());
            countdown = spawnWait;
            return;
        }
        countdown -= Time.deltaTime;

        countdown = Mathf.Clamp(countdown, 0f, Mathf.Infinity);
        waveCountdownText.text = string.Format("{0:00.00}", countdown);
    }

    IEnumerator SpawnWave()
    {
        PlayerStats.Rounds++;

        Wave wave = waves[waveIndex];
        waveIndex++;
        for (int i = 0; i < wave.count; i++)
        {
            // Hp of the monster scales up every wave
            // wave.enemy.GetComponent<EnemyStats>().maxHealth *= waveIndex;

            // Gold of the monster scales up every wave
            // wave.enemy.GetComponent<EnemyStats>().value *= waveIndex;

            SpawnEnemy(wave.enemy);
            yield return new WaitForSeconds(1f / wave.rate);
        }


        SpawnEnemy(BossGo);

        // Debug.Log("Wave Index: " + waveIndex);
        // Debug.Log("Wave Length: " + waves.Length);

        if (waveIndex == waves.Length)
        {
            Debug.Log("Game Finished");
            this.enabled = false;
        }

        // if (EnemiesAlive <= 0)
        // {
        //     Debug.Log("Game Finished");
        //     this.enabled = false;
        // }
    }

    void SpawnEnemy(GameObject enemy)
    {
        GameObject _enemy = Instantiate(enemy, spawnPoint.position, spawnPoint.rotation);
        _enemy.GetComponent<EnemyStats>().maxHealth *= waveIndex;
        _enemy.GetComponent<EnemyStats>().value *= waveIndex;
        EnemiesAlive++;
    }
}
